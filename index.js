function convertArray(arr, res, rej) {
    try {
        const n = arr.length;
        let result = arr.reduce((result, currentValue, index) => {
            if (!result.previousValue) {
                n === 1 ? result.str += currentValue : result.previousValue = currentValue;
                return result;
            }
            if (currentValue - result.previousValue === 1) {
                result.difference === 0 &&
                (result.str += `${result.first ? '' : ','}${result.previousValue}`);
                result.difference += 1;
            } else {
                result.difference > 1 ?
                    result.str += `-${result.previousValue}` :
                    result.str += `${result.first ? '' : ','}${result.previousValue}`;
                result.difference = 0;
            }
            result.previousValue = currentValue;
            index === 1 && (result.first = false);
            if (n - index === 1) {
                result.difference > 1 ?
                    result.str += `-${result.previousValue}` :
                    result.str += `${result.first ? '' : ','}${result.previousValue}`;
                result.difference = 0;
            }
            return result;
        }, { str: '', previousValue: null, first: true, difference: 0 });
        result = result.str;
        res(result);
    } catch (err) {
        rej(err);
    }
}

function exportDefault(arr) {
    return (new Promise((res, rej) => {
        convertArray(arr, res, rej);
    }));
}

module.exports = exportDefault;