const convertArray = require('../index');
const chai = require('chai');
const assert = chai.assert;
const arrayForTesting = [
    {
        test: [1, 2, 3, 4, 5, 6, 7, 8],
        result: '1-8',
    },
    {
        test: [1, 3, 4, 5, 6, 7, 8],
        result: '1,3-8',
    },
    {
        test: [1, 3, 4, 5, 6, 7, 8, 10, 11, 12],
        result: '1,3-8,10-12',
    },
    {
        test: [1, 2, 3],
        result: '1-3',
    },
    {
        test: [1, 2],
        result: '1,2',
    },
    {
        test: [1, 2, 4],
        result: '1,2,4',
    },
    {
        test: [1, 2, 4, 5, 6],
        result: '1,2,4-6',
    },
    {
        test: [1, 2, 3, 7, 8, 9, 15, 17, 19, 20, 21],
        result: '1-3,7-9,15,17,19-21',
    },
    {
        test: [1, 2, 3, 4, 5, 6, 100, 1091, 1999, 2000, 2001, 2002],
        result: '1-6,100,1091,1999-2002',
    },
    {
        test: [1],
        result: '1',
    },
    {
        test: [1, 3, 5, 7, 9, 11],
        result: '1,3,5,7,9,11',
    },
];

describe('Convert array', function() {
    for (let i = 0; i < arrayForTesting.length; ++i) {
        it(`Test #${i+1}`, async function() {
            const result = await convertArray(arrayForTesting[i].test);
            assert.equal(result, arrayForTesting[i].result, 'Converting fail');
        });
    }
});