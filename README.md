#converter array to string

`Converts an array of type [1, 2, 3, 5, 6, 7, 10] to string of type '1-3,5-7,10'`

##Installation

```
npm install converter-array-to-string
```

##Examples

```javascript
let converter = require('converter-array-to-string');

converter([1, 2, 5, 6, 7, 10])
    .then((result) => {
        console.log(result); // '1,2,5-7,10'
    });
```